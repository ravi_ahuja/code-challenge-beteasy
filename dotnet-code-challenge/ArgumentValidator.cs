﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dotnet_code_challenge
{
    public sealed class ArgumentValidator
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ArgumentValidator()
        {
        }

        internal String Validate(String[] args)
        {
            try
            {
                if (args.Length != 1)
                    throw new InvalidOperationException("The program needs the Full path to input file.");

                if (!File.Exists(args[0]))
                    throw new FileNotFoundException("The Full path supplied is NOT a valid file path.");

                // Get File Extension
                var namesplit = Path.GetFileName(args[0]).Split('.');
                if (namesplit.Length != 2)
                    throw new InvalidOperationException("The file cannot have period ('.') in it's Name");

                if (namesplit[1] != "xml" && namesplit[1] != "json")
                    throw new NotSupportedException("The Valid input files are only .json/.xml extension");

                return namesplit[1];
            }
            catch (Exception)
            {
                Console.WriteLine("Run the Program from CLI `dotnet dotnet_code_challenge.dll Fullpath of the input .json/.xml file`");
                throw;
            }

        }
    }
}
