﻿using Race.DataContract.Json.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace dotnet_code_challenge.Parsers
{
    public sealed class XmlFileReader : IFileReader
    {
        public XmlFileReader()
        {
        }

        public List<Result> ProcessPayload(String filepath)
        {
            // Read file contents as String
            var fileStr = File.ReadAllText(filepath).Trim();

            var result = new List<Result>();

            var inputDoc = new XmlDocument();
            inputDoc.LoadXml(fileStr);

            IDictionary<int, Result> horsesMap = new Dictionary<int, Result>();

            // Parse the Horses node to get horse details
            var horseNodes = inputDoc.SelectNodes("/meeting/races/race/horses/horse");
            if (horseNodes != null)
            {
                foreach (XmlNode horse in horseNodes)
                {
                    // Horse Name
                    var horseNameAttrib = horse.Attributes["name"];
                    var hName = horseNameAttrib.InnerText.Trim();
                    decimal price = 0;

                    var horseNumberNode = horse.FirstChild;
                    if (horseNumberNode != null)
                        horsesMap.Add(Convert.ToInt32(horseNumberNode.InnerText.Trim()), new Result(price, hName));
                }
            }

            // Parse the prices Nodes to get the fixing odds
            var horsesPriceNodes = inputDoc.SelectNodes("/meeting/races/race/prices/price/horses/horse");
            if (horsesPriceNodes != null)
            {
                foreach (XmlNode hPriceNode in horsesPriceNodes)
                {
                    var horsePriceAttrib = hPriceNode.Attributes["Price"];
                    var price = Convert.ToDecimal(horsePriceAttrib.InnerText.Trim());

                    var horseNoAttrib = hPriceNode.Attributes["number"];
                    var hNumber = Convert.ToInt32(horseNoAttrib.InnerText.Trim());

                    // Check the presence of horse in the Map and update the price value in Map.
                    Result oldHorseValue;
                    if (horsesMap.TryGetValue(hNumber, out oldHorseValue))
                    {
                        // Update Map
                        var res = new Result(price, oldHorseValue.HorserName);
                        horsesMap[hNumber] = res;

                        result.Add(res);
                    }
                }
            }

            // Sort list
            result = result.OrderBy(x => x.Price).ToList();

            return result;
        }
    }
}
