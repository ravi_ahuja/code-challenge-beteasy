﻿using Race.DataContract.Json.DataContract;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Parsers
{
    public interface IFileReader
    {
        List<Result> ProcessPayload(String filepath);
    }
}
