﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Race.DataContract.Json;
using Race.DataContract.Json.DataContract;

namespace dotnet_code_challenge.Parsers
{
    public sealed class JsonFileReader: IFileReader
    {
        public JsonFileReader()
        {
        }

        private RacePayloadJson Deserialize(String file)
        {
            return JsonConvert.DeserializeObject<RacePayloadJson>(file);
        }

        public List<Result> ProcessPayload(String filepath)
        {
            // Read file contents as String
            var fileStr = File.ReadAllText(filepath).Trim();

            // De-Serialize
            var racePayload = Deserialize(fileStr);

            var result = new List<Result>();

            // Linq query
            var seltns = from market in racePayload.RawData.Markets
                         select new { Selections = market.Selections.ToList() };

            foreach (var sels in seltns)
            {
                foreach (var item in sels.Selections)
                {
                    result.Add(new Result(item.Price, item.Tags.Name));
                }
            }

            // Sort list
            //var result = from elem in resList
            //             orderby elem.Price
            //             select elem;

            // Sort list
            result = result.OrderBy(x => x.Price).ToList();

            return result;
        }
    }
}
