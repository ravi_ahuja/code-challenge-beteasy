﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json.DataContract
{
    public class Result
    {
        public Result()
        {
        }

        public Result(decimal price, String name)
        {
            Price = price;
            HorserName = name;
        }

        public decimal Price { get; set; }
        public String HorserName { get; set; }
    }
}
