﻿using dotnet_code_challenge.Parsers;
using Race.DataContract.Json.DataContract;
using System;
using System.Collections.Generic;
using System.IO;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(String[] args)
        {
            try
            {
                // Validate input args.
                var inputCheck = new ArgumentValidator();
                var fileType = inputCheck.Validate(args);

                IFileReader reader = null;

                // Result 
                var resultList = new List<Result>();

                switch (fileType)
                {
                    case "json":
                        reader = new JsonFileReader();
                        resultList = reader.ProcessPayload(args[0]);
                        break;
                    case "xml":
                        reader = new XmlFileReader();
                        resultList = reader.ProcessPayload(args[0]);
                        break;
                    default:
                        break;
                }

                foreach (var item in resultList)
                {
                    Console.WriteLine($"Horse Name: {item.HorserName}, price: {item.Price}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error - Something went wrong: {ex.Message}");
#if DEBUG
                Console.WriteLine($"Stacktrace: {ex.StackTrace}");

                if (ex.InnerException != null)
                    Console.WriteLine($"Inner Exception: {ex.InnerException.Message}");
#endif
            }
        }
    }
}
