# .NET Code Challenge

You've been provided with the shell of a .Net application in Visual Studio with some sample inputs 

Create an application which outputs the horse names in price ascending order. 

The code should be at a standard you'd feel comfortable with putting in production.

## Background

The source data reflects how BetEasy has different providers of data which feed our website.

The data files are used allows creation of different races:
* Caulfield_Race1.xml https://beteasy.com.au/racing-betting/horse-racing/caulfield/20171216/race-1-798068-25502504  
* Wolferhampton_Race1.json https://beteasy.com.au/racing-betting/horse-racing/wolverhampton/20171213/race-1-797507-25500650

## Guidelines

You can either complete the test prior to the interview or come in and do it as part of the technical interview. 
The application shell provided is a suggestion only, if C#/.Net isn't your preferred language/Framework please use what you're most comfortable with.

## Assumptions
The file formats are currently restricted to .json/.xml
The sequence of order of data is in line with the sample files (json/xml) provided.
Any change in data order or format may not result in expected results


## Running the application
The tech stack here is primarily .net core 2.0
A .net core runtime is needed on test machine if the build and testing machines are different.
Download the source from repo and build it.
Go to project dotnet-code-challenge\bin folder and then under Debug/Release mode
Example --> dotnet-code-challenge\bin\Release\netcoreapp2.0

Execute the command line `dotnet dotnet_code_challenge.dll (Fullpath of the input .json/.xml file)`

## Unit tests
The test can be found 'dotnet-code-challenge.Test'
Us the Test option from the Menu and execute the tests (Run All tests) in Visual studio.
The test are at the moment very basic to test a sample inout json and xml file.

## P.S: 
For Simplicity reasons, the output includes the prices fixed along with the horse name to validate the order for larger data files on the standard output.

Thanks for giving me the opportunity :)