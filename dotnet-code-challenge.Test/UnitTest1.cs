using dotnet_code_challenge.Parsers;
using System;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Caulfield_Race1_xmlFileTest()
        {
            // Input
            var testFile = "C:\\Users\\ravi.ahuja\\Desktop\\FeedData\\Caulfield_Race1.xml";

            // Test
            var tst = new XmlFileReader();
            var result = tst.ProcessPayload(testFile);

            // Validate
            Assert.Equal(Convert.ToDecimal(4.2), result[0].Price);
            Assert.Equal("Advancing", result[0].HorserName);

            Assert.Equal(Convert.ToDecimal(12), result[1].Price);
            Assert.Equal("Coronel", result[1].HorserName);
        }

        [Fact]
        public void Wolferhampton_Race1_jsonFileTest()
        {
            // Input
            var testFile = "C:\\Users\\ravi.ahuja\\Desktop\\FeedData\\Wolferhampton_Race1.json";

            // Test
            var tst = new JsonFileReader();
            var result = tst.ProcessPayload(testFile);

            // Validate
            Assert.Equal(Convert.ToDecimal(4.4), result[0].Price);
            Assert.Equal("Fikhaar", result[0].HorserName);

            Assert.Equal(Convert.ToDecimal(10.0), result[1].Price);
            Assert.Equal("Toolatetodelegate", result[1].HorserName);
        }
    }
}
