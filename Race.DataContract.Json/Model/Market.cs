﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class Market
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public Market()
        {
            Selections = new List<Selection>();
        }

        public String Id { get; set; }
        public IEnumerable<Selection> Selections { get; set; }
        public MarketTags Tags { get; set; }
    }
}
