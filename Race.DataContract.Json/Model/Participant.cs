﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class Participant
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public Participant()
        {
        }

        public int Id { get; set; }
        public String Name { get; set; }
        public ParticipantTags Tags { get; set; }
    }
}
