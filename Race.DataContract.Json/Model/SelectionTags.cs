﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class SelectionTags
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public SelectionTags()
        {
        }

        public int Participant { get; set; }
        public String Name { get; set; }
    }
}
