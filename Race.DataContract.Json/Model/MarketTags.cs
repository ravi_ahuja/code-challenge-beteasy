﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class MarketTags
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public MarketTags()
        {
        }

        public int Places { get; set; }
        public String Type { get; set; }
    }
}
