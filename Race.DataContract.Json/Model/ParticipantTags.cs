﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class ParticipantTags
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public ParticipantTags()
        {
        }

        public String Weight { get; set; }
        public int Drawn { get; set; }
        public String Jockey { get; set; }
        public int Number { get; set; }
        public String Trainer { get; set; }
    }
}
