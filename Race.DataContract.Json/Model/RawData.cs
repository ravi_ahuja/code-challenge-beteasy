﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public class RawData
    {
        public RawData()
        {
            StartTime = new DateTime();
            Tags = new RawDataTags();

            Markets = new List<Market>();
            Participants = new List<Participant>();
        }

        public String FixtureName { get; set; }
        public String Id { get; set; }
        public DateTime StartTime { get; set; }
        public int Sequence { get; set; }
        public RawDataTags Tags { get; set; }
        public IEnumerable<Market> Markets { get; set; }
        public IEnumerable<Participant> Participants { get; set; }
    }
}
