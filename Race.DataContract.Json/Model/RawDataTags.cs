﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class RawDataTags
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public RawDataTags()
        {
        }

        public String CourserType { get; set; }
        public String Distance { get; set; }
        public String Going { get; set; }
        public int Runners { get; set; }
        public int MeetingCode { get; set; }
        public String TrackCode { get; set; }
        public String Sport { get; set; }
    }
}
