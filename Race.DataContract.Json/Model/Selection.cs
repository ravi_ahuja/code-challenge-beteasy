﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public sealed class Selection
    {
        /// <summary>
        /// Constrtuctor
        /// </summary>
        public Selection()
        {
        }

        public String Id { get; set; }
        public decimal Price { get; set; }
        public SelectionTags Tags{ get; set; }
    }
}
