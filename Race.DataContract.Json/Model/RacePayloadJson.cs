﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Race.DataContract.Json
{
    public class RacePayloadJson
    {
        public RacePayloadJson()
        {
            Timestamp = new DateTime();
        }

        public String FixtureId { get; set; }
        public DateTime Timestamp { get; set; }
        public RawData RawData { get; set; }
    }
}
